#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped
import tf
import numpy as np
#import copy
from grasp_control.srv import *

class GraspController:
    def __init__(self):
        rospy.init_node('grasp_control_node')
        #TO DO: make the joint state topic a ros node parameter.
        hand_joint_states_topic = '/allegro_hand_right/joint_states'
        rospy.Subscriber(hand_joint_states_topic, JointState, self.get_hand_joint_state)
        joint_cmd_topic = 'allegro_hand_right/joint_cmd'
        self.joint_cmd_pub = rospy.Publisher(joint_cmd_topic, JointState, queue_size=100)
        rate = 100
        self.loop_rate = rospy.Rate(rate)
        self.js_cmd = JointState()
        self.js_cmd.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
      
    def get_hand_joint_state(self, hand_js):
        self.cur_joint_states = hand_js 

    def publish_joint_cmd(self, js_cmd_pos):
        self.js_cmd.position = js_cmd_pos
        for i in xrange(10):
            self.joint_cmd_pub.publish(self.js_cmd)
            self.loop_rate.sleep()
 
    def grasp_preshape_control(self, req):
        stop_control = False
        control_times = 0
        min_control_times = 5
        while not stop_control:
            cur_js_pos = np.array(self.cur_joint_states.position)
            cur_js_vel = np.array(self.cur_joint_states.velocity)
            next_js_pos = np.copy(cur_js_pos)
            
            if req.grasp_type == 'prec':
                next_js_pos[1:3] += req.close_non_thumb_speed 
                next_js_pos[5:7] += req.close_non_thumb_speed 
                next_js_pos[9:11] += req.close_non_thumb_speed 
                next_js_pos[14:] += req.close_thumb_speed
            elif req.grasp_type == 'power':
                next_js_pos[1:4] += req.close_non_thumb_speed 
                next_js_pos[5:8] += req.close_non_thumb_speed 
                next_js_pos[9:12] += req.close_non_thumb_speed 
                next_js_pos[14:] += req.close_thumb_speed
            else:
                rospy.logerr('Wrong grasp type for grasp preshape controller!')

            #if control_times > min_control_times:
            #    for i in xrange(cur_js_vel.size):
            #        if abs(cur_js_vel[i]) < req.joint_vel_thresh:
            #            next_js_pos[i] = cur_js_pos[i]

            self.publish_joint_cmd(next_js_pos)
            #Check if all joints are in contact as stopping ceteria.
            in_contact = np.all(np.abs(cur_js_vel) < req.joint_vel_thresh)
            #print control_times, in_contact

            #stop_control = in_contact and control_times > min_control_times
            stop_control = in_contact or control_times > min_control_times
            control_times += 1

        #Increase hand joint angles to close harder.
        #rospy.sleep(1)
        control_harder_times = 5
        for i in xrange(control_harder_times):
            cur_js_pos = np.array(self.cur_joint_states.position)
            cur_js_vel = np.array(self.cur_joint_states.velocity)
            next_js_pos = np.copy(cur_js_pos)
            
            if req.grasp_type == 'prec':
                next_js_pos[1:3] += req.close_non_thumb_speed 
                next_js_pos[5:7] += req.close_non_thumb_speed
                next_js_pos[9:11] += req.close_non_thumb_speed
                next_js_pos[14:] += req.close_thumb_speed

                #Closing veolcity of precision grasp for lego is slow to prevent knocking lego off,
                #So, need to increase the closing speed here to make it tighter.
                #next_js_pos[1:3] += req.close_non_thumb_speed + 0.1 
                #next_js_pos[5:7] += req.close_non_thumb_speed + 0.1
                #next_js_pos[9:11] += req.close_non_thumb_speed + 0.1
                #next_js_pos[14:] += req.close_thumb_speed + 0.1
            elif req.grasp_type == 'power':
                next_js_pos[1:4] += req.close_non_thumb_speed 
                next_js_pos[5:8] += req.close_non_thumb_speed 
                next_js_pos[9:12] += req.close_non_thumb_speed 
                next_js_pos[14:] += req.close_thumb_speed
            else:
                rospy.logerr('Wrong grasp type for grasp preshape controller!')

            self.publish_joint_cmd(next_js_pos)

        response = PreshapeControlResponse()
        response.final_joint_states = self.cur_joint_states
        response.success = True
        return response

    def create_grasp_control_server(self):
        rospy.Service('grasp_control', PreshapeControl, self.grasp_preshape_control)
        rospy.loginfo('Service grasp_control:')
        rospy.loginfo('Ready to run grasp controller to close the preshape.')

if __name__ == '__main__':
    controller = GraspController()
    controller.create_grasp_control_server()
    rospy.spin()

